let sum a b = a +. b

let () =
    print_endline "sum 21.0 0.42 =";
    print_float (sum 21.0 0.42);
    print_endline "";
    print_endline "sum 37.12 0.0 =";
    print_float (sum 37.12 0.0);
    print_endline ""
