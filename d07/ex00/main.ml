(* main.ml *)

let () =
    let bill = new People.people "Bill Potts"
    in
    print_endline bill#to_string;
    bill#talk;
    bill#die
