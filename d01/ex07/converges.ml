let rec converges f x n =
    if n <= 0 then (x = (f x))
    else if x = (f x) then true
    else converges f (f x) (n - 1)

let () =
    let print_rec f x n =
        if (converges f x n) = true then
            print_endline "True"
        else
            print_endline "False"
    in
    print_rec (( * ) 2) 2 5;
    print_rec (fun x -> x / 2) 2 3;
    print_rec (fun x -> x / 2) 2 2;
    print_rec (( * ) 2) 2 1000000;
    print_rec (fun x -> x / 2) 2 (-1)
