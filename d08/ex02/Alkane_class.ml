(* Alkane_class.ml *)

class virtual alkane n =
    object (self)

        method name =
            match n with
            | 1 ->  "Methane"
            | 2 ->  "Ethane"
            | 3 ->  "Propane"
            | 4 ->  "Butane"
            | 5 ->  "Pentane"
            | 6 ->  "Hexane"
            | 7 ->  "Heptane"
            | 8 ->  "Octane"
            | 9 ->  "Nonane"
            | 10 -> "Decane"
            | 11 -> "Hendecane"
            | 12 -> "Dodecane"
            | _ ->  "Impossible"

        method formula =
            match n with
            | x when (x > 12 || x < 1) -> "Wrong alkane"
            | x -> "C" ^ (string_of_int n) ^ "H" ^ (string_of_int ((n * 2) + 2))

        method to_string =
            "Alkane -> " ^ self#name ^ " | " ^ self#formula

        method equals (another_alkane : alkane) =
            (compare self#formula another_alkane#formula)
    end
