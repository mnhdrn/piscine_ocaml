let encode lst =
    let rec do_enc lst n ret =
        match lst with
        | [] -> ret
        | h :: s :: t  -> (
            if h = s then
                do_enc (s :: t) (n + 1) ret
            else
                do_enc (s :: t) 0 (ret @ [(n + 1), h])
        )
        | h :: t  -> do_enc [] 0 (ret @ [(n + 1), h])
    in
    do_enc lst 0 []

let () =
    let print_ret (a, b) = Printf.printf "(%d, %d) " a b 
    in
    List.iter print_ret (encode [8;77;123;5678;12;5;5;5;6]);
    print_endline "";
    List.iter print_ret (encode [1;2;2;3;3;3;4;4;4;4;5;5;5;5;5]);
    print_endline "";
    List.iter print_ret (encode []);
    print_endline "";
    List.iter print_ret (encode [42]);
    print_endline "";
    List.iter print_ret (encode [42;(-1);42;1;3;8;3;1;42]);
    print_endline ""
(*    List.iter print_ret (encode ["hey"; "hey"; "oh"]);
    List.iter print_ret (encode ["a"; "b"; "b"; "o"; "a"]);
    List.iter print_ret (encode ["42"; ""; "42"]);
    List.iter print_ret (encode ["toto"]) *)

