
let () =
	let deck_of_card:Deck.t = Deck.newDeck in
	let rec print_deck li =
		match li with
		| [] -> ()
		| hd :: tl -> print_string (hd ^ " "); print_deck tl;
	in
	print_deck (Deck.toStringList deck_of_card);
	print_endline "\n";
	print_deck (Deck.toStringListVerbose deck_of_card);
	print_endline "\n"
