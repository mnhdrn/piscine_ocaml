(* eu_dist.ml *)

let eu_dist a b =
    if Array.length a != Array.length b then (-1.)
    else (
        let ret = ref 0. 
        in
        for i = 0 to ((Array.length a) - 1) do
            ret := !ret +. (((Array.get a i) -. (Array.get b i)) ** 2.)
        done;
        sqrt !ret
    )

let () =
	let first = Array.create_float 6 in
	let toto = Array.create_float 6 in
	let second = Array.create_float 2 in
	let third = Array.create_float 1 in
	let third2 = Array.create_float 1 in
	first.(0) <- 1.;
	first.(1) <- 1.;
	first.(2) <- 1.;
	first.(3) <- 1.;
	first.(4) <- 1.;
	first.(5) <- 1.;
	toto.(0) <- 1.;
	toto.(1) <- 1.;
	toto.(2) <- 1.;
	toto.(3) <- 1.;
	toto.(4) <- 1.;
	toto.(5) <- 2.;
	second.(0) <- 1.;
	second.(1) <- 1.;
	third.(0) <- 0.;
	third2.(0) <- 13.;
	print_float (eu_dist first toto);
	print_char '\n';
	print_float (eu_dist second second);
	print_char '\n';
	print_float (eu_dist third third2);
	print_char '\n';
	third.(0) <- 21.;
	third2.(0) <- 42.;
	print_float (eu_dist third third2);
	print_char '\n'
