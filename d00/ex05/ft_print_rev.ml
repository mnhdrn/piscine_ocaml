let ft_print_rev s =
    let rec do_rev s l =
        if l >= 0 then (
            print_char (String.get s l);
            do_rev s (l - 1)
            )
        else ()
    in
    do_rev s ((String.length s) - 1);
    print_char '\n'

let () =
    ft_print_rev "Hello World !";
    ft_print_rev ""
