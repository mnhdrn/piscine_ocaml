(* Molecule_element *)

class water =
    object (self)
        inherit Molecule_class.molecule "water" [
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.oxygen
        ]
    end

class carbon_dioxyde =
    object (self)
        inherit Molecule_class.molecule "carbon dioxyde" [
            new Atom_element.carbon;
            new Atom_element.oxygen;
            new Atom_element.oxygen
        ]
    end

class trinitrotoluene =
    object (self)
        inherit Molecule_class.molecule "trinitrotoluene" [
            new Atom_element.nitrogen;
            new Atom_element.nitrogen;
            new Atom_element.nitrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.oxygen;
            new Atom_element.oxygen;
            new Atom_element.oxygen;
            new Atom_element.oxygen;
            new Atom_element.oxygen;
            new Atom_element.oxygen;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
        ]
    end

class strychnine=
    object (self)
        inherit Molecule_class.molecule "cyanure" [
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.nitrogen;
            new Atom_element.nitrogen;
            new Atom_element.oxygen;
            new Atom_element.oxygen;
        ]
    end

class acetate=
    object (self)
        inherit Molecule_class.molecule "acetate" [
            new Atom_element.carbon;
            new Atom_element.carbon;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.hydrogen;
            new Atom_element.oxygen;
            new Atom_element.oxygen;
        ]
    end
