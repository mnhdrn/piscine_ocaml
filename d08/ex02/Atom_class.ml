(* Atom_class.ml *)

class virtual atom name symbol atomic_number =
   object (self)

       method name : string = name
       method symbol : string = symbol
       method atomic_number : int = atomic_number

       method to_string =
           "Atom -> " ^ self#name ^ " | " ^ self#symbol ^ " | " ^ (string_of_int self#atomic_number)

       method equals (another_atom : atom) =
           (compare self#atomic_number another_atom#atomic_number)

   end

