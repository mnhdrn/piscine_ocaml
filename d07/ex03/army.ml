(* army.ml *)

class ['a] army (member : 'a list) =
    object

        val member : 'a list = member

        method add (el : 'a) = {< member = el :: member >}

        method delete =
            let do_del member =
                match member with
                | [] -> []
                | h :: t -> t
            in
            {< member = do_del member >}
    end
