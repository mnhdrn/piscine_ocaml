(* Alkane_class_element *)

class methane =
    object (self)
        inherit Alkane_class.alkane 1
    end


class ethane =
    object (self)
        inherit Alkane_class.alkane 2
    end

class propane =
    object (self)
        inherit Alkane_class.alkane 3
    end

class butane =
    object (self)
        inherit Alkane_class.alkane 4
    end

class pentane =
    object (self)
        inherit Alkane_class.alkane 5
    end

class hexane =
    object (self)
        inherit Alkane_class.alkane 6
    end

class heptane =
    object (self)
        inherit Alkane_class.alkane 7
    end

class octane =
    object (self)
        inherit Alkane_class.alkane 8
    end

class nonane =
    object (self)
        inherit Alkane_class.alkane 9
    end

class dodecane =
    object (self)
        inherit Alkane_class.alkane 12
    end

