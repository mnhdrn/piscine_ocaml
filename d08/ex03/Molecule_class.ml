(* Molecule_class.ml *)

class virtual molecule name lst =
    object (self)

        method name : string = name
        method atom_list : Atom_class.atom list = lst

        method formula =
            let tmp = List.map (fun (x : Atom_class.atom) -> x#symbol) lst
            in
            let weight el =
                match el with
                | "C" -> -2
                | "H" -> -1
                | x -> int_of_char (String.get x 0)
            in
            let sort lst =
                let nd = List.map (fun s -> (weight s, s)) lst in
                let sd = List.sort compare nd in
                let und sd =
                    match sd with
                    | (i, s) -> s
                in
                List.map und sd
            in
            let encode lst =
                let do_ret ret h n =
                    if (n + 1) == 1 then
                        (ret ^ h)
                    else
                        (ret ^ h ^ (string_of_int (n + 1)))
                    in
                let rec do_enc lst n ret =
                    match lst with
                    | [] -> ret
                    | h :: s :: t  -> (
                        if h = s then
                            do_enc (s :: t) (n + 1) ret
                        else
                            do_enc (s :: t) 0 (do_ret ret h n)
                            )
                    | h :: t  -> do_enc [] 0 (do_ret ret h n)
                in
                do_enc lst 0 ""
            in
            encode (sort tmp)

        method to_string =
            "Molecule -> " ^ self#name ^ " | " ^ self#formula

        method equals (another_molecule: molecule) =
            (compare self#formula another_molecule#formula)

    end
