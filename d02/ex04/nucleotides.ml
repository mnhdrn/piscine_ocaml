type phosphate = string
type deoxyribose = string
type nucleobase = A | T | G | C | None

type nucleotide = {
    phosphate : phosphate;
    deoxyribose : deoxyribose;
    nucleobase : nucleobase;
}

let generate_nucleotide c = {
    phosphate = "phosphate";
    deoxyribose = "deoxyribose";
    nucleobase = match c with
                | 'A' -> A
                | 'T' -> T
                | 'C' -> C
                | 'G' -> G
                | _ -> None
}

let () =
    let test c =
        let dna = generate_nucleotide c
        in (
            "DNA: " ^ dna.phosphate ^
            " - " ^ dna.deoxyribose ^
            " - " ^ (
                match dna.nucleobase with
                | A -> "A"
                | T -> "T"
                | C -> "C"
                | G -> "G"
                | None -> "?"
            ))
   in
   Printf.printf "%s\n" (test 'A');
   assert (test 'A' = "DNA: phosphate - deoxyribose - A");
   Printf.printf "%s\n" (test 'T');
   assert (test 'T' = "DNA: phosphate - deoxyribose - T");
   Printf.printf "%s\n" (test 'C');
   assert (test 'C' = "DNA: phosphate - deoxyribose - C");
   Printf.printf "%s\n" (test 'G');
   assert (test 'G' = "DNA: phosphate - deoxyribose - G");
   Printf.printf "%s\n" (test 'Z');
   assert (test '?' = "DNA: phosphate - deoxyribose - ?")
