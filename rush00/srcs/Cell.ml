(* Cell.ml *)

type cell = {c : string; pos : int; index : int}

let initCell c i n = {c=c; pos=(succ i); index=n}

let getChar cell = cell.c

let getIndex cell = cell.index

let getPos cell = cell.pos

let toPlyOne cell = {c="O"; pos=cell.pos; index=cell.index}

let toPlyTwo cell = {c="X"; pos=cell.pos; index=cell.index}

let printCell cell =
    print_string cell.c

let printCellDebug cell =
    print_string cell.c;
        print_string " :: ";
        print_int cell.pos;
        print_string " :: ";
        print_int cell.index;
        print_endline ""

