(* Monoid.ml *)

module type MONOID =
    sig
        type element
        val zero1 : element
        val zero2 : element
        val add : element -> element -> element
        val sub : element -> element -> element
        val mul : element -> element -> element
        val div : element -> element -> element
    end

module INT =
    struct
        type element = int

        let zero1 = 0
        let zero2 = 1
        let add = ( + )
        let sub = ( - )
        let mul = ( * )
        let div = ( / )
    end

module FLOAT =
    struct
        type element = float

        let zero1 = 0.
        let zero2 = 1.
        let add = ( +. )
        let sub = ( -. )
        let mul = ( *. )
        let div = ( /. )
    end

module Calc =
    functor (M : MONOID) ->
        struct

            let add a b = M.add a b
            let sub a b = M.sub a b
            let mul a b = M.mul a b
            let div a b = M.div a b

            let rec power x = function
                | 0 -> M.zero1
                | 1 -> x
                | n when (n mod 2 = 0) -> let a = power x (n / 2) in (M.mul a a)
                | n -> let a = power x (n / 2) in M.mul x (M.mul a a)

            let rec fact (n:M.element) =
                match n with
                | (n:M.element) when (n <= M.zero2) -> M.zero2
                | _ -> M.mul n (fact (M.sub (n : M.element) M.zero2))

        end

