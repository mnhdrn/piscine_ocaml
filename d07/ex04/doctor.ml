(* doctor.ml *)

class doctor (name : string) (age : int) (sidekick : People.people) =
    object
        initializer print_endline "the doctor is born !"

        val name : string = name
        val mutable age : int = age
        val mutable hp : int= 100
        val sidekick : People.people = sidekick

        method to_string =
            "my name is " ^ name ^ ", I'm " ^ (string_of_int age) ^ " years old, I've got " ^ (string_of_int hp) ^ " hp. Have you met my sidekick ? come'on don't be shy introduce yourself: " ^ sidekick#to_string ^ ")"

        method talk =
            print_endline "Hello, I am the Doctor."

        method use_sonic_screwdriver =
            print_endline "Whiiiiwhiiiwhiii Whiiiiwhiiiwhiii Whiiiiwhiiiwhiii"

        method private regenerate = hp <- 100

        method travel_in_time (start : int) (destination : int) =
            let time =
                match (age + (destination - start)) with
                | x when x > 0 -> age + (destination - start)
                | _ -> age
            in 
            age <- time;
            print_endline ("        ___        ");
            print_endline ("_______(_@_)_______");
            print_endline ("| POLICE      BOX |");
            print_endline ("|_________________|");
            print_endline (" | _____ | _____ |");
            print_endline (" | |###| | |###| |");
            print_endline (" | |###| | |###| | ");
            print_endline (" | _____ | _____ |");
            print_endline (" | || || | || || |");
            print_endline (" | ||_|| | ||_|| |");
            print_endline (" | _____ |$_____ |");
            print_endline (" | || || | || || |");
            print_endline (" | ||_|| | ||_|| |");
            print_endline (" | _____ | _____ |");
            print_endline (" | || || | || || | ");
            print_endline (" | ||_|| | ||_|| | ");
            print_endline (" |       |       | ");
            print_endline (" *****************")
    end
