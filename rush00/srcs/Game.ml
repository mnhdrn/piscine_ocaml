(* Game.ml *)

let nextTurn ply =
    match ply with
    | "X" -> "O"
    | "O" -> "X"
    | _ -> failwith "Wrong player"

let checkMove pos map =
    match pos with
    | (_, a) -> (((Cell.getChar (List.nth map (pred a))) = "-"))

let playTurn board ply =
    print_endline (ply ^ "'s player turn");
    let f = match ply with | "O" -> Cell.toPlyOne | _ -> Cell.toPlyTwo 
    in
    let rec loop f board ply =
        let pos = Input.getInput (read_line ())
        in
        match pos with
        | (a, b) when (checkMove pos (List.nth board (pred a))) = true -> Board.stroke f board pos ply
        | (_, _) -> (
            print_endline "Unvalid stroke";
            loop f board ply
        )
    in
    loop f board ply

