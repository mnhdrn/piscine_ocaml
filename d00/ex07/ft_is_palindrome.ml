let ft_is_palindrome s =
    let l = String.length s
    in
    let rec loop i =
        if i < (l / 2) then
            String.get s i = String.get s (l - 1 - i) && loop (i + 1)
        else true
    in
    loop 0

let () =
    let bool_of_string s =
        if s == true then "True"
        else "False"
    in
    let check s = print_endline (bool_of_string (ft_is_palindrome s))
    in 
    check "radar";
    check "madam";
    check "car";
    check ""
