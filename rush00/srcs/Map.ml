(* Map.ml *)

type map = Cell.cell list

let initMap (n : int) : map =
    let rec loop i acc =
        match i with
            | 9 -> acc
            | _ -> loop (succ i) (acc @ ((Cell.initCell "-" i (succ n)) :: []))
    in
        loop 0 []

let getIndex map =
    match map with
    | [] -> -1
    | h :: t -> Cell.getIndex h

let toString map =
    let rec loop map i =
        match map with
        | [] -> print_endline "";
        | head :: tail when (i mod 3) != 0 ->
                Cell.printCell head; print_char ' ' ; loop tail (succ i)
        | head :: tail ->
                Cell.printCell head; print_char '\n' ; loop tail (succ i)
    in
        loop map 1

let rowToString map n =
    let a = n * 3 in
    let x = (Cell.getChar (List.nth map (a + 0))) in
    let y = (Cell.getChar (List.nth map (a + 1))) in
    let z = (Cell.getChar (List.nth map (a + 2)))
    in
    x ^ " " ^ y ^ " " ^ z

let stroke f map pos =
    let rec loop f map pos acc =
        match map with
        | [] -> acc
        | head :: tail when (Cell.getPos head) = pos -> loop f tail pos (acc @ (f head) :: [])
        | head :: tail -> loop f tail pos (acc @ head :: [])
    in
    loop f map pos []

let finishMap map ply =
    let str = if ply = "O" then "\\ / O / \\" else "\\ / X / \\"
    in
    let rec loop i str acc =
        match i with
            | 9 -> acc
            | _ -> loop (succ i) str (acc @ ((Cell.initCell (String.make 1 str.[i]) i (getIndex map)) :: []))
    in
        loop 0 str []

let isFinish map = (Cell.getChar (List.nth map 0) = "\\")

let getFinish map =
    if isFinish map = true then Cell.getChar(List.nth map 4)
    else "-"
